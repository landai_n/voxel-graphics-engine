#include	<ctime>
#include	<cstdlib>
#include	<iostream>
#include	"Window.hpp"
#include	"Context.hpp"
#include	"Keyboard.hpp"
#include	"Mouse.hpp"

using namespace	vge;

int			main()
{
  Window		window("Voxel Graphics Engine", Vector2(800, 600));
  Scene			&scene = window.CreateScene(10000);
  Event			e;
  Color			color(0xFFFFFFFF);

  scene.CreateCube(Vector3(100, 0, 100), 500);
  window.SetFrameLimit(60);
  while (window.IsOpen())
    {
      while (window.PopEvent(e))
          if (e.type == Event::KeyPressed && e.key.code == vge::KeyCode::Esc)
      	    window.Close();

      scene.Display();
    }
}

/*
**	vge::Mouse::IsPressed et Context::Draw utilises dans la meme boucle provoque une violente chute de FPS.
**	Conflit serveur X ?
*/

