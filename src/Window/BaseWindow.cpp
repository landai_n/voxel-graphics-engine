#include	<iostream>
#include	"BaseWindow.hpp"

namespace	vge
{
  BaseWindow::BaseWindow(const std::string &p_name, const Vector2 &p_size)
  {
    m_implem = IWindow::Create(p_name, p_size);
    m_frameLimit = 0;
    m_prevFrameTime = Chrono::now();
  }

  BaseWindow::~BaseWindow()
  {
    Close();
  }

  bool		BaseWindow::Close()
  {
    if (m_implem == nullptr)
      return (false);
    delete(m_implem);
    m_implem = nullptr;
    return (true);
  }

  bool		BaseWindow::IsOpen()
  {
    if (m_implem == nullptr)
      return (false);
    auto	elapsedTime = Chrono::now() - m_prevFrameTime;

    m_prevFrameTime = Chrono::now();
    if (m_frameLimit == 0)
      return (true);
    #ifndef VGE_WINDOWS
    elapsedTime %= 1000000 / m_frameLimit + 1;
    std::this_thread::sleep_for(std::chrono::microseconds(1000000 / m_frameLimit) - std::chrono::duration_cast<std::chrono::microseconds>(elapsedTime));
    #else
    elapsedTime %= 1000 / m_frameLimit;
    Sleep((std::chrono::milliseconds(1000 / m_frameLimit) - std::chrono::duration_cast<std::chrono::milliseconds>(elapsedTime)).count());
    #endif

    return (true);
  }

  int		BaseWindow::GetFramerate()
  {
    auto	elapsedTime = Chrono::now() - m_prevFrameTime;

    return (1000000 / std::chrono::duration_cast<std::chrono::microseconds>(elapsedTime).count());
  }

  void		BaseWindow::SetFrameLimit(unsigned int p_limit)
  {
    m_frameLimit = p_limit;
  }

  void		BaseWindow::SetName(const std::string &p_name)
  {
    if (m_implem != nullptr)
      m_implem->SetName(p_name);
  }

  void		BaseWindow::SetSize(const Vector2 &p_size)
  {
    if (m_implem != nullptr)
      m_implem->SetSize(p_size);
  }

  bool		BaseWindow::PopEvent(Event &p_event)
  {
    if (m_implem != nullptr)
      {
	m_implem->EnqueueEvents();
	return (m_implem->PopEvent(p_event));
      }
    return (false);
  }

  void		BaseWindow::Flush()
  {
    if (m_implem != nullptr)
      m_implem->Flush();
  }

  void		BaseWindow::Clear()
  {
    if (m_implem != nullptr)
      m_implem->Clear();
  }

  const Vector2	&BaseWindow::GetSize() const
  {
    if (m_implem != nullptr)
      return (m_implem->GetSize());
    throw IWindow::Error("BaseWindow::GetSize() : window's implementation does not exist.");
  }
}
