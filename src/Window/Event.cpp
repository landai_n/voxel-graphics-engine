#include	"Event.hpp"

namespace vge
{

  Event::Event(EventType p_type)
  {
    type = p_type;
  }

  Event		&Event::operator=(const Event &to_copy)
  {
    type = to_copy.type;
    key = to_copy.key;
    mouse = to_copy.mouse;
    return (*this);
  }

}
