#ifdef      VGE_WINDOWS

#include    <iostream>
#include    "WindowWin.hpp"

namespace vge
{

int WindowWin::m_count = 0;

WindowWin::WindowWin(const std::string &p_name, const Vector2 &p_size) : m_className("VGE_Window")
{
    IWindow::SetName(p_name);
    m_handle = GetModuleHandleW(NULL);
    if (m_count == 0)
        if (RegisterWindowClass() == false)
            throw vge::IWindow::Error("RegisterClassEx() failed.");

    m_window = CreateWindowEx(
                    WS_EX_CLIENTEDGE,
                    m_className,
                    p_name.c_str(),
                    WS_OVERLAPPEDWINDOW,
                    CW_USEDEFAULT,
                    CW_USEDEFAULT,
                    p_size.GetX(),
                    p_size.GetY(),
                    NULL,
                    NULL,
                    m_handle,
                    NULL);

    if (m_window == NULL)
        throw vge::IWindow::Error("CreateWindowEx() failed.");
    ShowWindow(m_window, SW_SHOWNORMAL);
    UpdateWindow(m_window);
    m_count++;
}

WindowWin::~WindowWin()
{
    if (m_window)
    {
        DestroyWindow(m_window);
    }
    m_count--;
}

void        WindowWin::SetSize(const Vector2 &p_size)
{


}

void        WindowWin::SetName(const std::string &p_name)
{

}

void        WindowWin::EnqueueEvents()
{
    MSG     msg;

    while (PeekMessageW(&msg, NULL, 0, 0, PM_REMOVE))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
}

void        WindowWin::Flush()
{

}

void        WindowWin::Clear()
{

}

bool        WindowWin::RegisterWindowClass()
{
    m_windowClass.cbSize        = sizeof(WNDCLASSEX);
    m_windowClass.style         = 0;
    m_windowClass.lpfnWndProc   = &WindowWin::WinProc;
    m_windowClass.cbClsExtra    = 0;
    m_windowClass.cbWndExtra    = 0;
    m_windowClass.hInstance     = m_handle;
    m_windowClass.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
    m_windowClass.hCursor       = LoadCursor(NULL, IDC_ARROW);
    m_windowClass.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
    m_windowClass.lpszMenuName  = NULL;
    m_windowClass.lpszClassName = m_className;
    m_windowClass.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);
    if (!RegisterClassEx(&m_windowClass))
        return (false);
    return (true);
}

LRESULT CALLBACK	WindowWin::WinProc(HWND p_window, UINT p_message, WPARAM p_wParam, LPARAM p_lParam)
{
  vge::Event	event;

  switch(p_message)
    {
    case WM_CLOSE:
      event.type = Event::CloseWindow;
      DestroyWindow(p_window);
      break;
    case WM_DESTROY:
      PostQuitMessage(0);
      break;
    case WM_KEYDOWN:
      event.type = Event::KeyPressed;
      event.key.code = KeyCode(p_wParam);
      break;
    case WM_KEYUP:
      event.type = Event::KeyReleased;
      event.key.code = KeyCode(p_wParam);
      break;
    case WM_LBUTTONDOWN:
        event.type = Event::ButtonPressed;
        event.mouse.button = ButtonCode::LeftButton;
        event.mouse.position = Vector2(LOWORD(p_lParam), HIWORD(p_lParam));
        break;
    case WM_LBUTTONUP:
        event.type = Event::ButtonReleased;
        event.mouse.button = ButtonCode::LeftButton;
        event.mouse.position = Vector2(LOWORD(p_lParam), HIWORD(p_lParam));
        break;
    case WM_RBUTTONDOWN:
        event.type = Event::ButtonPressed;
        event.mouse.button = ButtonCode::RightButton;
        event.mouse.position = Vector2(LOWORD(p_lParam), HIWORD(p_lParam));
        break;
    case WM_RBUTTONUP:
        event.type = Event::ButtonReleased;
        event.mouse.button = ButtonCode::RightButton;
        event.mouse.position = Vector2(LOWORD(p_lParam), HIWORD(p_lParam));
        break;
    case WM_MBUTTONDOWN:
        event.type = Event::ButtonPressed;
        event.mouse.button = ButtonCode::MiddleButton;
        event.mouse.position = Vector2(LOWORD(p_lParam), HIWORD(p_lParam));
        break;
    case WM_MBUTTONUP:
        event.type = Event::ButtonReleased;
        event.mouse.button = ButtonCode::MiddleButton;
        event.mouse.position = Vector2(LOWORD(p_lParam), HIWORD(p_lParam));
        break;
    case WM_MOUSEWHEEL:
        event.type = Event::MouseWheel;
        event.mouse.deltaWheel = p_wParam;
        event.mouse.position = Vector2(LOWORD(p_lParam), HIWORD(p_lParam));
        break;
    default:
      return DefWindowProc(p_window, p_message, p_wParam, p_lParam);
    }
   IWindow::eventList.push(event);
   return 0;
}

HWND		&WindowWin::GetWindow()
{
    return (m_window);
}

}
#endif // VGE_WINDOWS
