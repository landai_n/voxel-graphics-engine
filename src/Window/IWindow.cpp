#include	"IWindow.hpp"

#if defined(VGE_UNIX)
 #include "WindowUnix.hpp"
#endif
#if defined(VGE_WINDOWS)
 #include "WindowWin.hpp"
#endif

namespace	vge
{
  std::queue<Event>			IWindow::eventList;

  IWindow	*IWindow::Create(const std::string &p_name, const Vector2 &p_size)
  {
    return (new WindowImpl(p_name, p_size));
  }

  IWindow::~IWindow()
  {

  }

  void		IWindow::SetSize(const Vector2 &p_size)
  {
    m_size = p_size;
  }

  void		IWindow::SetName(const std::string &p_name)
  {
    m_name = p_name;
  }

  const Vector2	&IWindow::GetSize() const
  {
    return (m_size);
  }

  const std::string	&IWindow::GetName() const
  {
    return (m_name);
  }

  bool			IWindow::PopEvent(Event &p_event)
  {
    if (IWindow::eventList.empty())
      return (false);
    p_event = IWindow::eventList.front();
    IWindow::eventList.pop();
    return (true);
  }
}
