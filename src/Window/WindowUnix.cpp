#if defined(VGE_UNIX)

#include	<X11/keysym.h>
#include	<X11/Xutil.h>
#include	"WindowUnix.hpp"

namespace	vge
{

  WindowUnix::WindowUnix(const std::string &p_name, const Vector2 &p_size)
  {
    if (!(m_display = XOpenDisplay(nullptr)))
      throw vge::IWindow::Error("WindowUnix() : XOpenDisplay() failed.");
    m_blackValue = BlackPixel(m_display, DefaultScreen(m_display));
    m_whiteValue = WhitePixel(m_display, DefaultScreen(m_display));

    m_window = XCreateSimpleWindow(m_display,
				   DefaultRootWindow(m_display),
				   0,
				   0,
				   p_size.GetX(),
				   p_size.GetY(),
				   0,
				   m_blackValue,
				   m_blackValue);

    IWindow::SetSize(p_size);
    SetName(p_name);
    m_context = XCreateGC(m_display, m_window, 0, 0);
    XSetForeground(m_display, m_context, m_whiteValue);
    XSetBackground(m_display, m_context, m_blackValue);

    XSelectInput(m_display, m_window,
		 ButtonPressMask |
		 ButtonReleaseMask |
		 KeyReleaseMask |
		 KeyPressMask |
		 ExposureMask |
		 StructureNotifyMask);

    XMapWindow(m_display, m_window);

    XEvent	e;

    do
      {
	XNextEvent(m_display, &e);
      }
    while (e.type != MapNotify);
  }

  WindowUnix::~WindowUnix()
  {
    XDestroyWindow(m_display, m_window);
    XCloseDisplay(m_display);
  }

  void	WindowUnix::SetName(const std::string &p_name)
  {
    IWindow::SetName(p_name);
    XStoreName(m_display, m_window, m_name.c_str());
  }

  void	WindowUnix::SetSize(const Vector2 &p_size)
  {
    IWindow::SetSize(p_size);
  }

  void	WindowUnix::Flush()
  {
    XFlush(m_display);
  }

  void	WindowUnix::Clear()
  {
    XClearWindow(m_display, m_window);
  }

  void	WindowUnix::EnqueueEvents()
  {
    while (XPending(m_display))
      {
	XEvent		xEvent;
	vge::Event	event;
	KeySym		key;

	XNextEvent(m_display, &xEvent);

	switch (xEvent.type)
	  {
	  case KeyPress:
	    key = XLookupKeysym((XKeyEvent *)&xEvent, 0);
	    event.type = Event::KeyPressed;
	    event.key.code = KeyCode(key);
	    break;
	  case KeyRelease:
	    key = XLookupKeysym((XKeyEvent *)&xEvent, 0);
	    event.type = Event::KeyReleased;
	    event.key.code = KeyCode(key);
	    break;
	  case ButtonPress:
	    if (xEvent.xbutton.button == 4)
	      {
		event.type = Event::MouseWheel;
		event.mouse.position = Vector2(xEvent.xbutton.x, xEvent.xbutton.y);
		event.mouse.deltaWheel = 1;
	      }
	    else if (xEvent.xbutton.button == 5)
	      {
		event.type = Event::MouseWheel;
		event.mouse.button = ButtonCode(xEvent.xbutton.button);
		event.mouse.deltaWheel = -1;
	      }
	    else
	      {
		event.type = Event::ButtonPressed;
		event.mouse.button = ButtonCode(xEvent.xbutton.button);
		event.mouse.position = Vector2(xEvent.xbutton.x, xEvent.xbutton.y);
	      }
	    break;
	  case ButtonRelease:
	    event.type = Event::ButtonReleased;
	    event.mouse.button = ButtonCode(xEvent.xbutton.button);
	    event.mouse.position = Vector2(xEvent.xbutton.x, xEvent.xbutton.y);
	    break;
	  default:
	    event.type = Event::Unknown;
	    break;
	  }
	IWindow::eventList.push(event);
      }
  }

  ::Window		&WindowUnix::GetWindow()
  {
    return (m_window);
  }

  ::Display		*WindowUnix::GetDisplay()
  {
    return (m_display);
  }

  ::GC			&WindowUnix::GetGC()
  {
    return (m_context);
  }
}

#endif
