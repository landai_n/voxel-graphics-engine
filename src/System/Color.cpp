#include	"Color.hpp"

namespace	vge
{
  Color::Color(unsigned char p_red, unsigned char p_green, unsigned char p_blue, unsigned char p_alpha) :
    red(p_red), green(p_green), blue(p_blue), alpha(p_alpha)
  {

  }

  Color::Color(const Color &p_color) :
    red(p_color.red), green(p_color.green), blue(p_color.blue), alpha(p_color.alpha)
  {

  }

  Color::Color(unsigned long p_value)
  {
    SetValue(p_value);
  }

  unsigned long		Color::GetValue() const
  {
    union ColorValue	color;

    color.array[0] = red;
    color.array[1] = green;
    color.array[2] = blue;
    color.array[3] = alpha;

    return (color.value);
  }

  void			Color::SetValue(unsigned long p_value)
  {
    union ColorValue	color;

    color.value = p_value;
    red = color.array[0];
    green = color.array[1];
    blue = color.array[2];
    alpha = color.array[3];
  }
}
