#include	"Vector3.hpp"

namespace	vge
{
  Vector3::Vector3(size_t p_x, size_t p_y, size_t p_z) : Vector2(p_x, p_y)
  {
    z = p_z;
  }

  Vector3::Vector3(const Vector3 &p_vector) : Vector2(p_vector.x, p_vector.y), z(p_vector.z)
  {
  }

  size_t		Vector3::GetZ() const
  {
    return (z);
  }

  std::string Vector3::ToString() const
  {
    std::string str = "[" + std::to_string(x);
    
    str += ";" + std::to_string(y);
    str += ";" + std::to_string(z) + "]";
    return (str);    
  }

  Vector3	&Vector3::operator+=(const Vector3 &value)
  {
    x += value.GetX();
    y += value.GetY();
    z += value.GetZ();
    return (*this);
  }

  Vector3	&Vector3::operator-=(const Vector3 &value)
  {
    x -= value.GetX();
    y -= value.GetY();
    z -= value.GetZ();
    return (*this);
  }

  Vector3	operator+(const Vector3 &value1, const Vector3 &value2)
  {
    return (Vector3(value1.GetX() + value2.GetX(), value1.GetY() + value2.GetY(), value1.GetZ() + value2.GetZ()));
  }

  Vector3	operator-(const Vector3 &value1, const Vector3 &value2)
  {
    return (Vector3(value1.GetX() - value2.GetX(), value1.GetY() - value2.GetY(), value1.GetZ() - value2.GetZ()));
  }

  Vector3	operator*(const Vector3 &value1, const Vector3 &value2)
  {
    return (Vector3(value1.GetX() * value2.GetX(), value1.GetY() * value2.GetY(), value1.GetZ() * value2.GetZ()));
  }

  Vector3	operator/(const Vector3 &value1, const Vector3 &value2)
  {
    return (Vector3(value1.GetX() / value2.GetX(), value1.GetY() / value2.GetY(), value1.GetZ() / value2.GetZ()));
  }

  bool       operator==(const Vector3 &value1, const Vector3 &value2)
  {
    return (operator==(Vector2(value1), Vector2(value2)) && value1.z == value2.z);
  }

  std::ostream	&operator<<(std::ostream &os, const Vector3 &value)
  {
    os << "[" << value.GetX() << " ; " << value.GetY() << " ; " << value.GetZ() << "]";
    return (os);
  }
}
