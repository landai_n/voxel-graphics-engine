#include	"Vector2.hpp"

namespace	vge
{
  Vector2::Vector2(size_t p_x, size_t p_y)
  {
    x = p_x;
    y = p_y;
  }

  size_t		Vector2::GetX() const
  {
    return (x);
  }

  size_t		Vector2::GetY() const
  {
    return (y);
  }
  
  std::string Vector2::ToString() const
  {
    std::string str = "[" + std::to_string(x);
    
    str += ";" + std::to_string(y) + "]";
    return (str);    
  }

  Vector2	&Vector2::operator+=(const Vector2 &value)
  {
    x += value.GetX();
    y += value.GetY();
    return (*this);
  }

  Vector2	&Vector2::operator-=(const Vector2 &value)
  {
    x -= value.GetX();
    y -= value.GetY();
    return (*this);
  }

  Vector2	operator+(const Vector2 &value1, const Vector2 &value2)
  {
    return (Vector2(value1.GetX() + value2.GetX(), value1.GetY() + value2.GetY()));
  }

  Vector2	operator-(const Vector2 &value1, const Vector2 &value2)
  {
    return (Vector2(value1.GetX() - value2.GetX(), value1.GetY() - value2.GetY()));
  }

  Vector2	operator*(const Vector2 &value1, const Vector2 &value2)
  {
    return (Vector2(value1.GetX() * value2.GetX(), value1.GetY() * value2.GetY()));
  }

  Vector2	operator/(const Vector2 &value1, const Vector2 &value2)
  {
    return (Vector2(value1.GetX() / value2.GetX(), value1.GetY() / value2.GetY()));
  }
  
  bool       operator==(const Vector2 &value1, const Vector2 &value2)
  {
    return (value1.x == value2.x && value1.y == value2.y);
  }

  std::ostream	&operator<<(std::ostream &os, const Vector2 &value)
  {
    os << "[" << value.GetX() << " ; " << value.GetY() << "]";
    return (os);
  }
}
