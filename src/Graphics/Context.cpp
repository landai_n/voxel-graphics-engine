#include	"Context.hpp"
#include	"BaseWindow.hpp"

#ifdef		VGE_UNIX
#include	"ContextUnix.hpp"
#elif		VGE_WINDOWS
#include	"ContextWin.hpp"
#endif

namespace	vge
{

  Context::Context(IWindow *p_window)
  {
    m_buffer = IContext::Create(p_window);
    m_window = p_window;
    m_editMode = false;
  }

  Context::~Context()
  {
    Close();
  }

  bool		Context::Draw() const
  {
    if (m_buffer == nullptr)
      return (false);
    return (m_buffer->Draw());
  }

  bool		Context::SetPixel(const Color &p_color, const Vector2 &p_position)
  {
    if (m_buffer == nullptr)
      return (false);
    return (m_buffer->SetPixel(p_color, p_position.x, p_position.y));
  }

  Color		Context::GetPixelColor(const Vector2 &p_position) const
  {
    if (m_buffer == nullptr)
      return (false);
    return (false);
    return (m_buffer->GetPixelColor(p_position));
  }

  bool		Context::SetEditMode(bool p_mode)
  {
    bool	m_result;

    if (p_mode)
      {
	m_result = m_lock.try_lock();
	if (m_result)
	  m_editMode = true;
	return (m_result);
      }
    m_editMode = false;
    m_lock.unlock();
    return (true);
  }

  bool		Context::GetEditMode()
  {
    return (m_editMode);
  }

  void		Context::Close()
  {
    if (m_buffer != nullptr)
      delete (m_buffer);
    m_buffer = nullptr;
  }

}

