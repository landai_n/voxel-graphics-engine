#ifdef		VGE_UNIX

#include	<X11/Xutil.h>
#include	"ContextUnix.hpp"
#include	"IWindow.hpp"
#include	"WindowUnix.hpp"

namespace	vge
{

  ContextUnix::ContextUnix(IWindow *p_window)
  {
    ::Display	*display;

    m_window = (WindowImpl*)p_window;
    display = m_window->GetDisplay();
    m_size = m_window->GetSize();
    m_image = XShmCreateImage(display,
			      DefaultVisual(display, DefaultScreen(display)),
			      24,
			      ZPixmap,
			      NULL,
			      &m_shmInfo,
			      m_size.GetX(),
			      m_size.GetY());
    if (m_image == NULL)
      throw IWindow::Error("ContextUnix() : XCreateImage() failed.");
    m_shmInfo.shmid = shmget(IPC_PRIVATE, m_image->bytes_per_line * m_image->height,
			     IPC_CREAT | 0777 );
    if (m_shmInfo.shmid < 0)
      throw IWindow::Error("ContextUnix() : shmget() failed. Unable to allocate shared memory.");
    m_shmInfo.shmaddr = m_image->data = (char *)shmat(m_shmInfo.shmid, 0, 0);

    if (m_shmInfo.shmaddr == (char *) -1)
      throw IWindow::Error("ContextUnix() : shmat() failed. Unable to attach allocated memory.");

    XShmAttach(m_window->GetDisplay(), &m_shmInfo);
    XInitImage(m_image);
    m_imageWidth = m_image->width * 4;
  }

  ContextUnix::~ContextUnix()
  {
    XShmDetach(m_window->GetDisplay(), &m_shmInfo);
    shmdt(m_shmInfo.shmaddr);
  }

  bool		ContextUnix::SetPixel(const Color &p_color, const int &p_x, const int &p_y)
  {
    int		pos;

    pos = p_x * m_imageWidth  + p_y * 4;

    // put data manually into the array because XPutPixel is too slow
    m_image->data[pos] = p_color.red;
    m_image->data[pos + 1] = p_color.green;
    m_image->data[pos + 2] = p_color.blue;
    m_image->data[pos + 3] = p_color.alpha;

    return (true);
  }

  Color		ContextUnix::GetPixelColor(const Vector2 &p_position) const
  {
    return (Color(XGetPixel(m_image, p_position.GetX(), p_position.GetY())));
  }

  bool		ContextUnix::Draw() const
  {
    if (XShmPutImage(m_window->GetDisplay(), m_window->GetWindow(), m_window->GetGC(),
    		     m_image, 0, 0, 0, 0, m_size.GetX(), m_size.GetY(), False))
      return (false);
    return (true);
  }
}

#endif
