#include	"WindowUnix.hpp"
#include	"Window.hpp"

namespace	vge
{
  Window::Window(const std::string &p_name, const Vector2 &p_size) : BaseWindow(p_name, p_size)
  {

  }

  Window::~Window()
  {

  }

  Scene		&Window::CreateScene(size_t p_size)
  {
    Scene	*newScene = new Scene(m_implem, p_size);

    m_sceneArray.push_back(newScene);
    return (*newScene);
  }

  bool		Window::Close()
  {
    while (m_sceneArray.empty() == false)
      {
	Scene	*back;

	back = m_sceneArray.back();
	m_sceneArray.pop_back();
	back->Close();
      }
    return (BaseWindow::Close());
  }

};
