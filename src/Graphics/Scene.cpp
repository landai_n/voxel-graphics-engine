#include	"Scene.hpp"
#include	"BaseWindow.hpp"

size_t g_allocated = 0;

namespace	vge
{

  Scene::Scene(IWindow *p_window, size_t p_size)
  {
    size_t  rounded = 1;

    m_run = true;
    for (int i = 0; i < NBR_BUFFER_STEP; ++i)
      m_buffer[i] = new Context(p_window);
    m_computingThread = new std::thread(ComputingThreadLauncher, (void *)this);

    while (rounded < p_size)
      rounded = rounded << 1;
    m_matrix = new Chunk(rounded, Vector3(0, 0, 0));
  }

  Scene::~Scene()
  {
    for (int i = 0; i < NBR_BUFFER_STEP; ++i)
      delete (m_buffer[i]);
  }

  void		Scene::SetVoxel(const Vector3 &p_position, const Color &p_color)
  {
    m_matrix->SetVoxel(p_color, p_position);
  }

  void    Scene::CreateCube(const Vector3 &p_position, size_t size)
  {
    Color color(255, 0, 255);

    std::cout << "Creating cube..." << std::endl;
    for (size_t i = 0; i < size; ++i)
      for (size_t j = 0; j < size; ++j)
        {
          // Faces avant et arriere
          SetVoxel(Vector3(p_position.x + i, p_position.y + j, p_position.z), color);
          SetVoxel(Vector3(p_position.x + i, p_position.y + j, p_position.z + size), color);

          // Face dessous et dessus
          SetVoxel(Vector3(p_position.x + i, p_position.y, p_position.z + j), color);
          SetVoxel(Vector3(p_position.x + i, p_position.y + size, p_position.z + j), color);

          // Face droite et gauche
          SetVoxel(Vector3(p_position.x, p_position.y + i, p_position.z + j), color);
          SetVoxel(Vector3(p_position.x + size, p_position.y + i, p_position.z + j), color);
        }
    std::cout << "Done. (Allocated " << g_allocated / 1048576 << " MB)" << std::endl;
  }

  void		Scene::Display()
  {
    m_buffer[FRONT_BUFFER]->Draw();
    if (m_buffer[BACK_BUFFER]->GetEditMode() == false)
      PushBuffers();
  }

  void		Scene::PushBuffers()
  {
    Context	*tmp = m_buffer[FRONT_BUFFER];

    for (int i = 0; i < NBR_BUFFER_STEP - 1; ++i)
      m_buffer[i] = m_buffer[i + 1];
    m_buffer[BACK_BUFFER] = tmp;
  }

  void		Scene::Close()
  {
    m_run = false;
    m_computingThread->join();
    for (int i = 0; i < NBR_BUFFER_STEP; i++)
      m_buffer[i]->Close();
  }

  void		Scene::ComputingThreadLauncher(void *args)
  {
    Scene	*scene = (Scene *)args;

    scene->ComputeBuffers();
  }

  void		Scene::ComputeBuffers()
  {
    while (m_run)
      {
	     for (int i = FRONT_BUFFER; i < BACK_BUFFER; ++i)
	     {
	       m_buffer[i]->SetEditMode(true);
	       // do many things
	       m_buffer[i]->SetEditMode(false);
	     }
      }
  }
}
