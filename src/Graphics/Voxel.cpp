#include	"Voxel.hpp"

namespace vge
{

  Voxel::Voxel(const Color &p_color) : m_color(p_color)
  {
  }

  Voxel::Voxel(const Voxel &p_voxel) :
    m_color(p_voxel.m_color)
  {

  }

  Voxel::~Voxel()
  {

  }

  const Color &Voxel::GetColor(__attribute__((unused)) const Vector3 &p_position)
  {
    return (m_color);
  }

  void Voxel::SetVoxel(const Color &p_color, __attribute__((unused)) const Vector3 &p_position)
  {
    m_color = p_color;
  }

  bool Voxel::Contain(__attribute__((unused)) const Vector3 &p_position)
  {
    return (true);
  }

}
