#include	"IContext.hpp"

# ifdef		VGE_UNIX
# include	"ContextUnix.hpp"
# elif		VGE_WINDOWS
# include	"ContextWin.hpp"
# endif

namespace	vge
{
  IContext	*IContext::Create(IWindow *p_window)
  {
    return (new ContextImpl(p_window));
  }
}
