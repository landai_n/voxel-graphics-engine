#include	<stdexcept>
#include	"Chunk.hpp"
#include	"Vector3.hpp"
#include	"Color.hpp"
#include	"Voxel.hpp"

namespace vge
{
  Chunk::Chunk(int p_size, const Vector3 &p_position)
  {
    m_size = p_size;
    m_position = p_position;
    m_childSize = m_size >> NB_CHUNK_SPLIT;
  }

  Chunk::~Chunk()
  {
    for (ChunkEntityMap::iterator x = m_content.begin(); x != m_content.end(); ++x)
      for (ChunkEntityMapY::iterator y = x->second.begin(); y != x->second.end(); ++y)
	for (ChunkEntityMapZ::iterator z = y->second.begin(); z != y->second.end(); ++z)
	  delete (z->second);
  }

  const Color	&Chunk::GetColor(const Vector3 &p_position)
  {
    ChunkEntity *entity;

    if ((entity = this->ContentAtPosition(p_position)) == nullptr)
      throw std::runtime_error("Trying to access inexistant ChunkEntity");
    return (entity->GetColor(p_position));
  }

  const Vector3 &Chunk::GetPosition() const
  {
    return (m_position);
  }

  void		Chunk::SetVoxel(const Color &p_color, const Vector3 &p_position)
  {
    ChunkEntity *entity;
    const Vector3 &position = p_position;

    if (this->Fit(position) == false)
      throw std::runtime_error("Trying to set forbidden position "
			       + position.ToString() + " in chunk at " + m_position.ToString()
			       + " of size " + std::to_string(m_size));
    if ((entity = this->ContentAtPosition(position)) == nullptr)
      this->CreateChild(p_color, p_position);
    else
      entity->SetVoxel(p_color, p_position);
  }

  void		Chunk::CreateChild(const Color &p_color, const Vector3 &p_position)
  {
    Vector3 position = this->GetChildPosition(p_position);

    if (m_childSize > 1)
      {
	m_content[position.x][position.y][position.z] = new Chunk(m_childSize, position);
	m_content[position.x][position.y][position.z]->SetVoxel(p_color, p_position);
	g_allocated += sizeof(Chunk);
      }
    else
      {
	m_content[position.x][position.y][position.z] = new Voxel(p_color);
	g_allocated += sizeof(Voxel);
      }
    g_allocated += sizeof(ChunkEntityMapZ);
  }

  Vector3		Chunk::GetChildPosition(const Vector3 &p_position) const
  {
    Vector3	position;

    if (p_position.x > m_childSize)
      while (position.x < p_position.x - m_childSize)
	position.x += m_childSize;
    if (p_position.y > m_childSize)
      while (position.y < p_position.y - m_childSize)
	position.y += m_childSize;
    if (p_position.z > m_childSize)
      while (position.z < p_position.z - m_childSize)
	position.z += m_childSize;
    return (position);
  }

  ChunkEntity	*Chunk::ContentAtPosition(const Vector3 &p_position)
  {
    Vector3	position = this->GetChildPosition(p_position);

    if (this->Contain(p_position) == false)
      return (nullptr);
    return (m_content[position.x][position.y][position.z]);
  }

  bool		Chunk::Contain(const Vector3 &p_position)
  {
    Vector3	position = this->GetChildPosition(p_position);

    if (m_content.find(position.x) != m_content.end())
      if (m_content[position.x].find(position.y) != m_content[position.x].end())
	if (m_content[position.x][position.y].find(position.z) !=
	    m_content[position.x][position.y].end())
	  return (true);
    return (false);
  }

  bool		Chunk::Fit(const Vector3 &p_position) const
  {
    if (p_position.x < m_position.x || p_position.x > m_position.x + m_size)
      return (false);
    if (p_position.y < m_position.y || p_position.y > m_position.y + m_size)
      return (false);
    if (p_position.z < m_position.z || p_position.z > m_position.z + m_size)
      return (false);
    return (true);
  }

}
