#include	"IInput.hpp"

# ifdef		VGE_UNIX
# include	"InputUnix.hpp"
# elif		VGE_WINDOWS
# include	"InputWin.hpp"
# endif

namespace	vge
{
  InputImpl	IInput::implementation;

  InputImpl	*IInput::GetImplementation()
  {
    return (&implementation);
  }

}
