#ifdef      VGE_WINDOWS

#include    "InputWin.hpp"
#include    "WindowWin.hpp"
#include    "BaseWindow.hpp"

namespace   vge
{
    InputWin::InputWin()
    {

    }

    InputWin::~InputWin()
    {

    }

    Vector2 InputWin::GetMousePosition() const
    {
        POINT p;

        if (!GetCursorPos(&p))
            return (Vector2());
        return (Vector2(p.x, p.y));
    }

    Vector2 InputWin::GetMousePosition(Window &p_window) const
    {
        WindowWin		*implem = (WindowWin*)(p_window.m_implem); //unsafe cast but p_window should always be a WindowWin
        POINT           p;

        if (implem != NULL)
            if (GetCursorPos(&p))
                if (ScreenToClient(implem->GetWindow(), &p))
                    return (Vector2(p.x, p.y));
        return (Vector2());
    }

    void    InputWin::SetMousePosition(const Vector2 &p_position)
    {
        SetCursorPos(p_position.GetX(), p_position.GetY());
    }

    void    InputWin::SetMousePosition(const Vector2 &p_position, Window &p_window)
    {
        WindowWin		*implem = (WindowWin*)(p_window.m_implem); //unsafe cast but p_window should always be a WindowWin
        POINT   p;

        if  (implem == NULL)
            return;
        p.x = p_position.GetX();
        p.y = p_position.GetY();
        ClientToScreen(implem->GetWindow(), &p);
        SetCursorPos(p.x, p.y);
    }

    bool    InputWin::KeyIsPressed(const KeyCode &p_key) const
    {
        return (GetAsyncKeyState(p_key) & 0x8000);
    }

    bool    InputWin::ButtonIsPressed(const ButtonCode &p_button) const
    {
        return (GetAsyncKeyState(p_button) & 0x8000);
    }
}

#endif
