#ifdef		VGE_UNIX

#include	<iostream>
#include	<X11/Xlib.h>
#include	"InputUnix.hpp"
#include	"BaseWindow.hpp"
#include	"WindowUnix.hpp"

namespace	vge
{
  InputUnix::InputUnix()
  {
    if (!(m_display = XOpenDisplay(nullptr)))
      throw IWindow::Error("InputUnix() : XOpenDisplay() failed.");
  }

  InputUnix::~InputUnix()
  {
    if (m_display)
      XCloseDisplay(m_display);
  }

  Vector2	InputUnix::GetMousePosition() const
  {
    int			rootX, rootY, winX, winY;
    unsigned int	mask;
    ::Window		root, child;

    XQueryPointer(m_display, DefaultRootWindow(m_display), &root, &child, &rootX, &rootY, &winX, &winY, &mask);

    return (Vector2(rootX, rootY));
  }

  Vector2	InputUnix::GetMousePosition(vge::BaseWindow &p_window) const
  {
    int			rootX, rootY, winX, winY;
    unsigned int	mask;
    ::Window		root, child;
    WindowUnix		*implem = (WindowUnix*)(p_window.m_implem); //unsafe cast but p_window should always be a WindowUnix

    if (implem == NULL)
      return (Vector2());
    XQueryPointer(m_display, implem->GetWindow(), &root, &child, &rootX, &rootY, &winX, &winY, &mask);

    return (Vector2(winX, winY));
  }

  void		InputUnix::SetMousePosition(const Vector2 &p_position)
  {
    XWarpPointer(m_display, None, DefaultRootWindow(m_display), 0, 0, 0, 0, p_position.GetX(), p_position.GetY());
    XFlush(m_display);
  }

  void		InputUnix::SetMousePosition(const Vector2 &p_position, BaseWindow &p_window)
  {
    WindowUnix		*implem = (WindowUnix*)(p_window.m_implem); //unsafe cast but p_window should always be a WindowUnix

    if (implem == NULL)
      return;
    XWarpPointer(m_display, None, implem->GetWindow(), 0, 0, 0, 0, p_position.GetX(), p_position.GetY());
    XFlush(m_display);
  }

  bool		InputUnix::KeyIsPressed(const KeyCode &p_key) const
  {
    ::KeyCode	Xkey = XKeysymToKeycode(m_display, KeySym(p_key));
    char	keyList[32];

    if (Xkey == 0)
      return (false);
    XQueryKeymap(m_display, keyList);

    return ((keyList[Xkey / 8] & (1 << (Xkey % 8))) != 0);
  }

  bool			InputUnix::ButtonIsPressed(const ButtonCode &p_button) const
  {
    int			rootX, rootY, winX, winY;
    unsigned int	mask;
    ::Window		root, child;

    XQueryPointer(m_display, DefaultRootWindow(m_display), &root, &child, &rootX, &rootY, &winX, &winY, &mask);

    switch (p_button)
      {
      case ButtonCode::LeftButton:	return (mask & Button1Mask);
      case ButtonCode::MiddleButton:	return (mask & Button2Mask);
      case ButtonCode::RightButton:	return (mask & Button3Mask);
      default:				return (false);
      }
  }

}

#endif
