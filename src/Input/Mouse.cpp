#include	"Mouse.hpp"

# ifdef		VGE_UNIX
# include	"InputUnix.hpp"
# elif		VGE_WINDOWS
# include	"InputWin.hpp"
# endif

namespace	vge
{

  InputImpl	*Mouse::impl = IInput::GetImplementation();

  bool		Mouse::IsPressed(const ButtonCode &p_button)
  {
    if (impl != NULL)
      return (impl->ButtonIsPressed(p_button));
    return (false);
  }

  Vector2	Mouse::GetPosition()
  {
    if (impl != NULL)
      return (impl->GetMousePosition());
    return (Vector2());
  }

  Vector2	Mouse::GetPosition(BaseWindow &p_window)
  {
    if (impl != NULL)
      return (impl->GetMousePosition(p_window));
    return (Vector2());
  }

  void		Mouse::SetPosition(const Vector2 &p_position)
  {
    if (impl != NULL)
      impl->SetMousePosition(p_position);
  }

  void		Mouse::SetPosition(const Vector2 &p_position, BaseWindow &p_window)
  {
    if (impl != NULL)
      impl->SetMousePosition(p_position, p_window);
  }
}
