#include	"Keyboard.hpp"

# ifdef		VGE_UNIX
# include	"InputUnix.hpp"
# elif		VGE_WINDOWS
# include	"InputWin.hpp"
# endif

namespace	vge
{

  InputImpl	*Keyboard::impl = IInput::GetImplementation();

  bool		Keyboard::IsPressed(const KeyCode &p_key)
  {
    if (impl != NULL)
      return (impl->KeyIsPressed(p_key));
    return (false);
  }
}
