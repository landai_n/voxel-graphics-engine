NAME		=		voxelEngine

SRCWINDOW	=		src/Window/IWindow.cpp		\
				src/Window/BaseWindow.cpp	\
				src/Window/Event.cpp		\
				src/Window/WindowUnix.cpp	\

SRCINPUT	=		src/Input/Keyboard.cpp			\
				src/Input/Mouse.cpp			\
				src/Input/IInput.cpp			\
				src/Input/InputUnix.cpp			\

SRCGRAPHICS	=		src/Graphics/ContextUnix.cpp		\
				src/Graphics/IContext.cpp		\
				src/Graphics/Context.cpp		\
				src/Graphics/Window.cpp			\
				src/Graphics/Scene.cpp			\
				src/Graphics/Chunk.cpp			\
				src/Graphics/Voxel.cpp			\


SRCSYSTEM	=		src/System/Vector2.cpp		\
				src/System/Vector3.cpp		\
				src/System/Color.cpp		\

SRC		=		src/main.cpp
SRC		+=		$(SRCSYSTEM)
SRC		+=		$(SRCWINDOW)
SRC		+=		$(SRCGRAPHICS)
SRC		+=		$(SRCINPUT)

OBJ		=		$(SRC:.cpp=.o)

INCLUDE		=		-I include/			\
				-I include/Window		\
				-I include/Input		\
				-I include/System		\
				-I include/Graphics		\

LDFLAGS		=		-lX11 -lm -lXext		\

CPPFLAGS	=		-W -Wall -Wextra -Wshadow -J 8 -std=c++11 $(INCLUDE)
CPPFLAGS	+=		-pthread
CPPFLAGS	+=		-D VGE_UNIX

CXX		=		g++

all:		$(NAME)

$(NAME):	$(OBJ)
		$(CXX) $(OBJ) -o $(NAME) $(CPPFLAGS) $(LDFLAGS)

clean:
		rm -f $(OBJ)

fclean:		clean
		rm -f $(NAME)

re:		fclean all

debug:		CPPFLAGS += -g
debug:		all

opti:		CPPFLAGS += -floop-interchange -floop-strip-mine -floop-block -ftree-loop-distribution -O3
opti:		all
