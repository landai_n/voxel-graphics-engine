#ifndef		MOUSEEVENT_HPP
# define	MOUSEEVENT_HPP

#include    "Vector2.hpp"
#include	"Input.hpp"

namespace	vge
{
  class		MouseEvent
  {
  public:
    MouseEvent() : button(ButtonCode::NoButton), deltaWheel(0) {}
    ButtonCode	button;
    Vector2     position;
    int         deltaWheel;
  };

};

#endif
