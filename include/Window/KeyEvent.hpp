#ifndef		KEYEVENT_HPP
# define	KEYEVENT_HPP

#include	"Input.hpp"

namespace	vge
{
  class		KeyEvent
  {
  public:
    KeyCode	code;
  };

};

#endif
