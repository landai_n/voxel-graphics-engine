#ifndef		IWINDOW_HPP
# define	IWINDOW_HPP

#include	<memory>
#include	<string>
#include	<stdexcept>
#include	<queue>
#include	"Config.hpp"
#include	"Vector2.hpp"
#include	"Event.hpp"

namespace	vge
{

#if defined(VGE_UNIX)
  class WindowUnix;
  typedef WindowUnix WindowImpl;
#endif

#if defined(VGE_WINDOWS)
  class WindowWin;
  typedef WindowWin WindowImpl;
#endif

class					IWindow
{
public:
  static IWindow			*Create(const std::string &p_name, const Vector2 &p_size);
  virtual				~IWindow();
  virtual const Vector2			&GetSize() const;
  virtual void				SetSize(const Vector2 &p_size);
  virtual void				SetName(const std::string &p_name);
  virtual const std::string		&GetName() const;
  virtual void				Flush() = 0;
  virtual void				Clear() = 0;
  virtual void				EnqueueEvents() = 0;
  bool					PopEvent(Event &p_event);

  class					Error : public std::exception
  {
  public:
    Error(const std::string &p_what) throw() : m_what(p_what) {}
    virtual const char			*what() const throw() { return (m_what.c_str()); }
    virtual     			~Error() throw() { }
  private:
    const std::string			m_what;
  };
protected:
    Vector2		    		m_size;
    std::string				m_name;
    static std::queue<Event>		eventList;
};

}

#endif
