#ifndef		WINDOWUNIX_HPP
# define	WINDOWUNIX_HPP

#if defined(VGE_UNIX)

#include	<X11/Xlib.h>
#include	"IWindow.hpp"

namespace	vge
{

  class					WindowUnix : public IWindow
  {
  public:
    WindowUnix(const std::string &p_name, const Vector2 &p_size);
    ~WindowUnix();
    virtual void			SetName(const std::string &p_name);
    virtual void			SetSize(const Vector2 &p_size);
    virtual void			EnqueueEvents();
    virtual void			Flush();
    virtual void			Clear();
    ::Window				&GetWindow();
    ::Display				*GetDisplay();
    ::GC				&GetGC();
  private:
    ::Window				m_window;
    unsigned long			m_blackValue;
    unsigned long			m_whiteValue;
    ::Display				*m_display;
    ::GC				m_context;
  };

}
#endif

#endif
