#ifndef WINDOWWIN_HPP
#define WINDOWWIN_HPP

#ifdef VGE_WINDOWS

#include <queue>
#include <windows.h>
#include "IWindow.hpp"

namespace vge
{

class   WindowWin : public IWindow
{
public:
    WindowWin(const std::string &p_name, const Vector2 &p_size);
    ~WindowWin();
    virtual void			SetName(const std::string &p_name);
    virtual void			SetSize(const Vector2 &p_size);
    virtual void			EnqueueEvents();
    virtual void			Flush();
    virtual void			Clear();
    bool                    RegisterWindowClass();
    static LRESULT CALLBACK WinProc(HWND p_window, UINT p_message, WPARAM p_wParam, LPARAM p_lParam);
    HWND	    			&GetWindow();
private:
    static int              m_count;
    WNDCLASSEX              m_windowClass;
    HWND                    m_window;
    const char*             m_className;
    HINSTANCE               m_handle;
};

}
#endif

#endif
