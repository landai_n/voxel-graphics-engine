#ifndef		BASEWINDOW_HPP
# define	BASEWINDOW_HPP

#ifdef		VGE_WINDOWS
#include	<windows.h>
#else
#include	<thread>
#endif

#include	"IWindow.hpp"
#include	"IContext.hpp"

# define	VGE_DEFAULT_FRAMELIMIT	60

namespace	vge
{

class		BaseWindow
{
  friend Vector2		InputImpl::GetMousePosition(vge::BaseWindow&) const;
  friend void			InputImpl::SetMousePosition(const vge::Vector2 &, vge::BaseWindow&);
public:
  BaseWindow(const std::string &p_name, const Vector2 &p_size);
  ~BaseWindow();
  bool				IsOpen();
  void				SetFrameLimit(unsigned int p_limit);
  virtual bool			Close();
  int				GetFramerate();

  //		Implementation wrappers		//
  void				SetName(const std::string &p_name);
  void				SetSize(const Vector2 &p_size);
  const Vector2			&GetSize() const;
  bool				PopEvent(Event &p_event);
  void				Flush();
  void				Clear();

protected:
  IWindow		*m_implem;
  Chrono::time_point	m_prevFrameTime;
  unsigned int		m_frameLimit;
};

}

#endif
