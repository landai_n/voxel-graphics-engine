#ifndef		EVENT_HPP
# define	EVENT_HPP

#include	"Input.hpp"
#include	"MouseEvent.hpp"
#include	"KeyEvent.hpp"

namespace	vge
{

  class		Event
  {
  public:
    typedef enum
      {
	CloseWindow,
	KeyPressed,
	KeyReleased,
	ButtonPressed,
	ButtonReleased,
	MouseWheel,
	Unknown,
	NoEvent
      }		EventType;
    Event(EventType p_type = NoEvent);
    Event	&operator=(const Event &to_copy);
    EventType	type;
    KeyEvent	key;
    MouseEvent	mouse;
  };

}

#endif
