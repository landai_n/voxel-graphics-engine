#ifndef		COLOR_HPP
# define	COLOR_HPP

namespace	vge
{

class	       	Color
{
public:
  Color(unsigned char p_red, unsigned char p_green, unsigned char p_blue, unsigned char p_alpha = 255);
  Color(unsigned long p_value);
  Color(const Color &p_color);
  unsigned long	GetValue() const;
  void		SetValue(unsigned long p_value);

  union		ColorValue
  {
    unsigned char array[4];
    unsigned long value;
  };

  unsigned char	red;
  unsigned char	green;
  unsigned char	blue;
  unsigned char	alpha;
};

}
#endif
