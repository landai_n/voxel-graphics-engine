#ifndef		VECTOR3_HPP
# define	VECTOR3_HPP

#include	"Vector2.hpp"

namespace	vge
{

class		Vector3 : public Vector2
{
public:
  Vector3(size_t p_x = 0, size_t p_y = 0, size_t p_z = 0);
  Vector3(const Vector3 &);
  size_t		z;
  size_t		GetZ() const;
  Vector3	&operator+=(const Vector3 &value);
  Vector3	&operator-=(const Vector3 &value);
  std::string ToString() const;
};

  Vector3       operator+(const Vector3 &value1, const Vector3 &value2);
  Vector3       operator-(const Vector3 &value1, const Vector3 &value2);
  Vector3       operator*(const Vector3 &value1, const Vector3 &value2);
  Vector3       operator/(const Vector3 &value1, const Vector3 &value2);
  bool          operator==(const Vector3 &value1, const Vector3 &value2);
  std::ostream  &operator<<(std::ostream &os, const Vector3 &value);

}
#endif
