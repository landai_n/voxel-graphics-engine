#ifndef		VECTOR2_HPP
# define	VECTOR2_HPP

#include	<iostream>

namespace	vge
{

class		Vector2
{
public:
  Vector2(size_t p_x = 0, size_t p_y = 0);
  size_t		GetX() const;
  size_t		GetY() const;
  size_t		x;
  size_t		y;
  Vector2	&operator+=(const Vector2 &value);
  Vector2	&operator-=(const Vector2 &value);
  std::string ToString() const;
};

  Vector2	operator+(const Vector2 &value1, const Vector2 &value2);
  Vector2	operator-(const Vector2 &value1, const Vector2 &value2);
  Vector2	operator*(const Vector2 &value1, const Vector2 &value2);
  Vector2	operator/(const Vector2 &value1, const Vector2 &value2);
  bool operator==(const Vector2 &value1, const Vector2 &value2);
  std::ostream	&operator<<(std::ostream &os, const Vector2 &value);
}
#endif
