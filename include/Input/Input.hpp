#ifndef		INPUT_HPP
# define	INPUT_HPP

#if defined(VGE_UNIX)
#include "InputUnix.hpp"
#endif

#if defined(VGE_WINDOWS)
#include "InputWin.hpp"
#endif

#endif
