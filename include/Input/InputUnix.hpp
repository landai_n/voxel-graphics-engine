#ifndef		INPUTUNIX_HPP
# define	INPUTUNIX_HPP

#ifdef		VGE_UNIX

#include	"IInput.hpp"
#include	"InputDefUnix.hpp"

typedef struct _XDisplay Display;

namespace		vge
{

class			InputUnix : public IInput
{
public:
  InputUnix();
  virtual		~InputUnix();
  virtual Vector2	GetMousePosition() const;
  virtual Vector2	GetMousePosition(BaseWindow &p_window) const;
  virtual void		SetMousePosition(const Vector2 &p_position);
  virtual void		SetMousePosition(const Vector2 &p_position, BaseWindow &p_window);
  virtual bool		KeyIsPressed(const KeyCode &p_key) const;
  virtual bool		ButtonIsPressed(const ButtonCode &p_button) const;
private:
  ::Display		*m_display;
};

}
#endif

#endif
