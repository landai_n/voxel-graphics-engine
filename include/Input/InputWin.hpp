#ifndef		INPUTWIN_HPP
# define	INPUTWIN_HPP

#ifdef      VGE_WINDOWS

#include    "IInput.hpp"
#include	"InputDefWin.hpp"

namespace   vge
{

class		InputWin : public IInput
{
public:
    InputWin();
  virtual		~InputWin();
  virtual Vector2	GetMousePosition() const;
  virtual Vector2	GetMousePosition(BaseWindow &p_window) const;
  virtual void		SetMousePosition(const Vector2 &p_position);
  virtual void		SetMousePosition(const Vector2 &p_position, BaseWindow &p_window);
  virtual bool		KeyIsPressed(const KeyCode &p_key) const;
  virtual bool		ButtonIsPressed(const ButtonCode &p_button) const;
};

}
#endif

#endif
