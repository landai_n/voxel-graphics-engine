#ifndef		MOUSE_HPP
# define	MOUSE_HPP

#include	"IInput.hpp"

namespace	vge
{

class			Mouse
{
public:
  static bool		IsPressed(const ButtonCode &p_button);
  static Vector2	GetPosition();
  static Vector2	GetPosition(BaseWindow &p_window);
  static void		SetPosition(const Vector2 &p_position);
  static void		SetPosition(const Vector2 &p_position, BaseWindow &p_window);
private:
  static InputImpl	*impl;
};

}

#endif
