#ifndef		KEYBOARD_HPP
# define	KEYBOARD_HPP

#include	"IInput.hpp"

namespace	vge
{

class			Keyboard
{
public:
  static bool		IsPressed(const KeyCode &p_key);
private:
  static InputImpl	*impl;
};

}

#endif
