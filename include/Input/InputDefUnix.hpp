#ifndef     INPUTUNIXDEF_HPP
# define    INPUTUNIXDEF_HPP

#if defined(VGE_UNIX)

#include <X11/keysym.h>

namespace   vge
{
typedef enum
  {
    Left	= XK_Left,
    Right	= XK_Right,
    Up		= XK_Up,
    Down	= XK_Down,
    Esc		= XK_Escape,
    Tab		= XK_Tab,
    Ins		= XK_Insert,
    Home	= XK_Home,
    PageUp	= XK_Page_Up,
    PageDown	= XK_Page_Down,
    End		= XK_End,
    Del		= XK_Delete,
    Backspace	= XK_BackSpace,
    Return	= XK_Return,
    Enter	= XK_KP_Enter,
    ShiftL	= XK_Shift_L,
    ShiftR	= XK_Shift_R,
    Caps	= XK_Caps_Lock,
    CtrlL	= XK_Control_L,
    CtrlR	= XK_Control_R,
    AltL	= XK_Alt_L,
    AltR	= XK_Alt_R,
    F1		= XK_F1,
    F2		= XK_F2,
    F3		= XK_F3,
    F4		= XK_F4,
    F5		= XK_F5,
    F6		= XK_F6,
    F7		= XK_F7,
    F8		= XK_F8,
    F9		= XK_F9,
    F10		= XK_F10,
    F11		= XK_F11,
    F12		= XK_F12,
    PrintScreen	= XK_Print,
    NumLock	= XK_Num_Lock,
    Pause	= XK_Pause,
    SuperL	= XK_Super_L,
    SuperR	= XK_Super_R,
    Minus	= XK_KP_Subtract,
    Plus	= XK_KP_Add,
    Mul		= XK_KP_Multiply,
    Divid	= XK_KP_Divide,
    Equal	= XK_KP_Equal,
    Space	= XK_space,
    Num0	= XK_0,
    Num1	= XK_1,
    Num2	= XK_2,
    Num3	= XK_3,
    Num4	= XK_4,
    Num5	= XK_5,
    Num6	= XK_6,
    Num7	= XK_7,
    Num8	= XK_8,
    Num9	= XK_9,
    A		= XK_A,
    B		= XK_B,
    C		= XK_C,
    D		= XK_D,
    E		= XK_E,
    F		= XK_F,
    G		= XK_G,
    H		= XK_H,
    I		= XK_I,
    J		= XK_J,
    K		= XK_K,
    L		= XK_L,
    M		= XK_M,
    N		= XK_N,
    O		= XK_O,
    P		= XK_P,
    Q		= XK_Q,
    R		= XK_R,
    S		= XK_S,
    T		= XK_T,
    U		= XK_U,
    V		= XK_V,
    W		= XK_W,
    X		= XK_X,
    Y		= XK_Y,
    Z		= XK_Z
  }		    KeyCode;

typedef enum
  {
    LeftButton		= 1,
    MiddleButton	= 2,
    RightButton		= 3,
    NoButton
  }			ButtonCode;
}

#endif // defined

#endif // INPUTUNIX_HPP
