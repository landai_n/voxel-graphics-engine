#ifndef		IINPUT_HPP
# define	IINPUT_HPP

# ifdef		VGE_UNIX
# include	"InputDefUnix.hpp"
# elif		VGE_WINDOWS
# include	"InputDefWin.hpp"
# endif
# include	"Vector2.hpp"

namespace	vge
{

#ifdef		VGE_UNIX
  class		InputUnix;
  typedef	InputUnix InputImpl;
#elif		VGE_WINDOWS
  class		InputWin;
  typedef	InputWin InputImpl;
#endif

class BaseWindow;

class			IInput
{
public:
  static InputImpl	*GetImplementation();
  virtual Vector2	GetMousePosition() const = 0;
  virtual Vector2	GetMousePosition(BaseWindow &p_window) const = 0;
  virtual void		SetMousePosition(const Vector2 &p_position) = 0;
  virtual void		SetMousePosition(const Vector2 &p_position, BaseWindow &p_window) = 0;
  virtual bool		KeyIsPressed(const KeyCode &p_key) const = 0;
  virtual bool		ButtonIsPressed(const ButtonCode &p_button) const = 0;
  virtual ~IInput() {}
private:
  static InputImpl	implementation;
};

}

#endif
