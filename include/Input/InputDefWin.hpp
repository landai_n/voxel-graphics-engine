#ifndef     INPUTDEFWIN_HPP
# define    INPUTDEFWIN_HPP

#if defined(VGE_WINDOWS)

#include <windows.h>

namespace vge
{
  typedef enum
    {
      Left		= VK_LEFT,
      Right		= VK_RIGHT,
      Up		= VK_UP,
      Down		= VK_DOWN,
      Esc		= VK_ESCAPE,
      Tab		= VK_TAB,
      Ins		= VK_INSERT,
      Home		= VK_HOME,
      PageUp		= VK_PRIOR,
      PageDown		= VK_NEXT,
      End		= VK_END,
      Del		= VK_DELETE,
      Backspace		= VK_BACK,
      Return		= VK_RETURN,
      Enter		= VK_RETURN,
      ShiftL		= VK_SHIFT,
      ShiftR		= VK_SHIFT,
      Caps		= VK_CAPITAL,
      CtrlL		= VK_CONTROL,
      CtrlR		= VK_CONTROL,
      AltL		= VK_MENU,
      AltR		= VK_MENU,
      F1		= VK_F1,
      F2		= VK_F2,
      F3		= VK_F3,
      F4		= VK_F4,
      F5		= VK_F5,
      F6		= VK_F6,
      F7		= VK_F7,
      F8		= VK_F8,
      F9		= VK_F9,
      F10		= VK_F10,
      F11		= VK_F11,
      F12		= VK_F12,
      PrintScreen	= VK_PRINT,
      NumLock		= VK_NUMLOCK,
      Pause		= VK_PAUSE,
      SuperL		= VK_LWIN,
      SuperR		= VK_RWIN,
      Minus		= 0x0,
      Plus		= VK_ADD,
      Mul		= VK_MULTIPLY,
      Divid		= VK_DIVIDE,
      Equal		= 0x0,
      Space		= VK_SPACE,
      Num0		= 0x0,
      Num1		= VK_OEM_1,
      Num2		= VK_OEM_2,
      Num3		= VK_OEM_3,
      Num4		= VK_OEM_4,
      Num5		= VK_OEM_5,
      Num6		= VK_OEM_6,
      Num7		= VK_OEM_7,
      Num8		= VK_OEM_8,
      Num9		= 0x0,
      A			= 0x41,
      B			= 0x42,
      C			= 0x43,
      D			= 0x44,
      E			= 0x45,
      F			= 0x46,
      G			= 0x47,
      H			= 0x48,
      I			= 0x49,
      J			= 0x4A,
      K			= 0x4B,
      L			= 0x4C,
      M			= 0x4D,
      N			= 0x4E,
      O			= 0x4F,
      P			= 0x50,
      Q			= 0x51,
      R			= 0x52,
      S			= 0x53,
      T			= 0x54,
      U			= 0x55,
      V			= 0x56,
      W			= 0x57,
      X			= 0x58,
      Y			= 0x59,
      Z			= 0x5A
    }		    KeyCode;

  typedef enum
    {
      LeftButton	= VK_LBUTTON,
      MiddleButton	= VK_MBUTTON,
      RightButton	= VK_RBUTTON,
      NoButton
    }			ButtonCode;
}

#endif

#endif
