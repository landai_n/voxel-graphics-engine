#ifndef		CONTEXT_HPP
# define	CONTEXT_HPP

# include	<mutex>
# include	"IContext.hpp"

namespace	vge
{

class		IWindow;

class		Context
{
public:
  void		Close();
  Context(IWindow *p_window);
  ~Context();
  bool		Draw() const;
  bool		SetEditMode(bool p_mode);
  bool		SetPixel(const Color &p_color, const Vector2 &p_position);
  Color		GetPixelColor(const Vector2 &p_position) const;
  bool		GetEditMode();
private:
  IContext	*m_buffer;
  std::mutex	m_lock;
  IWindow	*m_window;
  bool		m_editMode;
};

}

#endif
