#ifndef		CHUNCK_HPP
#define		CHUNCK_HPP

#include	<map>
#include	"ChunkEntity.hpp"

#define		NB_CHUNK_SPLIT	2 // binary operation

namespace	vge
{

typedef		std::map<size_t, std::map<size_t, std::map<size_t, ChunkEntity*>>> ChunkEntityMap;
typedef		std::map<size_t, std::map<size_t, ChunkEntity*>> ChunkEntityMapY;
typedef		std::map<size_t, ChunkEntity*> ChunkEntityMapZ;

class			Chunk : public ChunkEntity
{
public:
  Chunk(int p_size, const Vector3 &p_position);
  virtual		~Chunk();
  virtual const Color	&GetColor(const Vector3 &p_position);
  const Vector3		&GetPosition() const;
  virtual void		SetVoxel(const Color &, const Vector3 &);
  virtual bool		Contain(const Vector3 &);
private:
  void			CreateChild(const Color &, const Vector3 &);
  Vector3		GetChildPosition(const Vector3 &p_position) const;
  ChunkEntity		*ContentAtPosition(const Vector3 &p_position);
  bool			Fit(const Vector3 &p_position) const;

  ChunkEntityMap	m_content;
  Vector3		m_position;
  size_t		m_size;
  size_t		m_childSize;
};

}

#endif
