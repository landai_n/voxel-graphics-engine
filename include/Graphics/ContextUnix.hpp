#ifndef   CONTEXTUNIX_HPP
#define   CONTEXTUNIX_HPP

#ifdef		VGE_UNIX

#include	<X11/Xlib.h>
#include	<sys/shm.h>
#include	<sys/ipc.h>
#include	<X11/extensions/XShm.h>
#include	"IContext.hpp"
#include	"IWindow.hpp"

namespace	vge
{
  class			ContextUnix : public IContext
  {
  public:
    ContextUnix(IWindow *p_window);
    virtual		     ~ContextUnix();
    virtual bool    SetPixel(const Color &p_color, const int &p_x, const int &p_y);
    virtual Color	  GetPixelColor(const Vector2 &p_position) const;
    virtual bool	  Draw() const;
  private:
    ::XImage		    *m_image;
    Vector2         m_size;
    WindowImpl      *m_window;
    XShmSegmentInfo m_shmInfo;
    int			        m_imageWidth;
  };
}

#endif  // VGE_UNIX

#endif
