#ifndef		ICONTEXT_HPP
# define	ICONTEXT_HPP

#include	"Color.hpp"
#include	"Vector2.hpp"

namespace	vge
{

# ifdef         VGE_UNIX
  class         ContextUnix;
  typedef       ContextUnix ContextImpl;
# elif          VGE_WINDOWS
  class         ContextWin;
  typedef       ContextWin ContextImpl;
# endif

  class			IWindow;

  class			IContext
  {
  public:
    static IContext	*Create(IWindow *p_window);
    virtual bool	SetPixel(const Color &p_color, const int &p_x, const int &p_y) = 0;
    virtual Color	GetPixelColor(const Vector2 &p_position) const = 0;
    virtual bool	Draw() const = 0;
    virtual ~IContext() {}
  };
}

#endif
