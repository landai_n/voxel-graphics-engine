#ifndef				CHUNKENTITY_HPP
#define				CHUNKENTITY_HPP

#include			"Vector3.hpp"
#include			"Color.hpp"

extern size_t g_allocated;

namespace vge
{

class					ChunkEntity
{
public:
  virtual ~ChunkEntity() {}
  virtual const Color	&GetColor(const Vector3 &) = 0;
  virtual void SetVoxel(const Color &, const Vector3 &) = 0;
  virtual bool Contain(const Vector3 &) = 0;
};

}

#endif
