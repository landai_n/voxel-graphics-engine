#ifndef		WINDOW_HPP
# define	WINDOW_HPP

#include	<vector>
#include	"BaseWindow.hpp"
#include	"Scene.hpp"

namespace	vge
{
  class				Window : public BaseWindow
  {
  public:
    Window(const std::string &p_name, const Vector2 &p_size);
    virtual			~Window();
    Scene			&CreateScene(size_t p_size);
    virtual bool		Close();
  private:
    std::vector<Scene*>		m_sceneArray;
  };
}

#endif
