#ifndef		VOXEL_HPP
# define	VOXEL_HPP

# include "ChunkEntity.hpp"

namespace	vge
{

class     Voxel : public ChunkEntity
{
public:
  Voxel(const Color &p_color);
  Voxel(const Voxel &p_voxel);
  virtual ~Voxel();
  virtual const Color &GetColor(const Vector3 &p_position);
  virtual void SetVoxel(const Color &, const Vector3 &);
  virtual bool Contain(const Vector3 &);
private:
  Color   m_color;
};

}

#endif
