#ifndef		SCENE_HPP
# define	SCENE_HPP

# include	<thread>
# include	"Vector3.hpp"
# include	"Context.hpp"
# include	"Chunk.hpp"
# include	"Voxel.hpp"

# define	NBR_BUFFER_STEP	2
# define	FRONT_BUFFER	0
# define	BACK_BUFFER	1

namespace	vge
{
  class		IWindow;

  class		Scene
  {
  public:
    Scene(IWindow *p_window, size_t p_size);
    ~Scene();
    void  SetVoxel(const Vector3 &p_position, const Color &p_color);
    void  CreateCube(const Vector3 &p_position, size_t size);
    void	Display();
    void	Close();
    void	PushBuffers();
  private:
    std::thread	*m_computingThread;
    static void	ComputingThreadLauncher(void *);
    void	ComputeBuffers();
    Chunk	*m_matrix;
    Context	*m_buffer[NBR_BUFFER_STEP];
    bool	m_run;
  };

}

#endif
