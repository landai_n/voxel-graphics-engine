#ifndef		CONFIG_HPP
# define	CONFIG_HPP

# include	<chrono>

#ifdef _WIN64
# define    VGE_WINDOWS

#elifdef _WIN32
# define    VGE_WINDOWS
#endif

# define	MAX_RAM_AVAILABLE	6442450944	// octets

typedef	std::chrono::steady_clock Chrono;

#endif
